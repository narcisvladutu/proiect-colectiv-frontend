import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { ProductComponent } from './components/products/product/product.component';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';

import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';

import { MatButtonModule } from '@angular/material/button';
import { RecipesComponent } from './components/recipes/recipes.component';
import { MyShoppingListComponent } from './components/my-shopping-list/my-shopping-list.component';
import { HeaderComponent } from './components/header/header.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddProductToShoppingListComponent } from './components/products/add-product-to-shopping-list/add-product-to-shopping-list.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ProductsEffects } from './store/products/products-effects';
import { productsReducer } from './store/products/products-reducers';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductComponent,
    RecipesComponent,
    MyShoppingListComponent,
    HeaderComponent,
    AddProductToShoppingListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatDialogModule,
    StoreModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot(
      {
        products: productsReducer,
      },
      {}
    ),
    EffectsModule.forRoot([ProductsEffects]),
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
