import { Product } from './Product';

export interface Company {
  id: number;
  email: string;
  username: string;
  password: string;
  image: string;
  products: Product[];
}
