export enum Path {
  PRODUCTS = '/products',
  RECIPES = '/recipes',
  MY_SHOPPING_LIST = '/my-shopping-list',
}
