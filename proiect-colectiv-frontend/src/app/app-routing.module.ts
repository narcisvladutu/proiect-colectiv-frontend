import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyShoppingListComponent } from './components/my-shopping-list/my-shopping-list.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { RecipesComponent } from './components/recipes/recipes.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductListComponent,
  },
  {
    path: 'recipes',
    component: RecipesComponent,
  },
  {
    path: 'my-shopping-list',
    component: MyShoppingListComponent,
  },
  {
    path: '',
    redirectTo: '/products',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
