import { Component, OnInit } from '@angular/core';
import { Company } from 'src/app/models/Company';
import { Product } from 'src/app/models/Product';
import { CompaniesService } from 'src/app/services/companies.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[] | undefined;
  companies: Company[] | undefined;

  constructor(
    private productService: ProductService,
    private companiesService: CompaniesService
  ) {}

  ngOnInit(): void {
    this.productService.getAllProducts().subscribe((products) => {
      this.products = products;
    });

    this.companiesService.getAllCompanies().subscribe((companies) => {
      this.companies = companies;
    });
  }

  displayAllProducts() {
    this.productService.getAllProducts().subscribe((products) => {
      this.products = products;
    });
  }

  displayProductsByCompany(company: Company) {
    this.productService.getProductsByCompany(company).subscribe((products) => {
      this.products = products;
    });
  }
}
