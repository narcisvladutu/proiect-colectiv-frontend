import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/models/Product';
import { AppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { AddProductToShoppingListComponent } from '../add-product-to-shopping-list/add-product-to-shopping-list.component';
import { addProductToShoppingList } from 'src/app/store/products/products-actions';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  @Input()
  product: Product | undefined;
  @Input()
  addedToShoppingList: boolean = false;

  constructor(private dialog: MatDialog, private store: Store<AppState>) {}

  ngOnInit(): void {}

  addProductToMyShoppingList(): void {
    let dialogRef = this.dialog.open(AddProductToShoppingListComponent, {
      height: '200px',
      width: '400px',
      enterAnimationDuration: '400ms',
      exitAnimationDuration: '300ms',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'DA') this.saveProduct();
    });
  }

  saveProduct() {
    this.store.dispatch(
      addProductToShoppingList({
        product: this.product
          ? this.product
          : {
              id: 0,
              name: '',
              price: 0,
              image: '',
              expire_date: '',
              stock: 0,
              companyName: '',
            },
      })
    );
  }

  playSound() {
    console.log('da');
    let audio = new Audio();
    audio.src = '../../../assets/melodie.mp4';
    audio.load();
    audio.play();
  }
}
