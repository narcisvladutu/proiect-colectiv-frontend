import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/models/Ingredient';
import { IngredientService } from 'src/app/services/ingredient.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit {
  ingredients: Ingredient[] | undefined;

  constructor(private ingredientService: IngredientService) {}

  ngOnInit(): void {
    this.ingredientService.getAllIngredients().subscribe((ingredients) => {
      this.ingredients = ingredients;
    });
  }
}
