import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { Product } from 'src/app/models/Product';
import {
  selectAllProducts,
  selectProducts,
} from 'src/app/store/products/products-selectors';

@Component({
  selector: 'app-my-shopping-list',
  templateUrl: './my-shopping-list.component.html',
  styleUrls: ['./my-shopping-list.component.scss'],
})
export class MyShoppingListComponent implements OnInit {
  myProducts: Product[] = [];

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select(selectAllProducts).subscribe((products) => {
      this.myProducts = products;
    });
  }

  hasProducts(): boolean {
    return this.myProducts.length !== 0;
  }
}
