import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '../models/Company';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>('http://localhost:8081/products');
  }

  getProductsByCompany(company: Company): Observable<Product[]> {
    return this.http.get<Product[]>(
      'http://localhost:8081/products/company=' +
        encodeURIComponent(company.username)
    );
  }
}
