import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';

@Injectable()
export class ProductsEffects {
  constructor(private actions$: Actions, private store: Store<AppState>) {}
}
