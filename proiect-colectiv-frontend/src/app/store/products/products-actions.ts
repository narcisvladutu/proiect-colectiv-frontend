import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/models/Product';

export const addProductToShoppingList = createAction(
  '[Products] Add Product To Shopping List',
  props<{ product: Product }>()
);

export const addProductToShoppingListSuccess = createAction(
  '[Products] Add Product To Shopping List Success',
  props<{ product: Product }>()
);
