import { Product } from 'src/app/models/Product';

export interface IProductsState {
  products: Product[];
}

export const initialProductsState: IProductsState = {
  products: [],
};
