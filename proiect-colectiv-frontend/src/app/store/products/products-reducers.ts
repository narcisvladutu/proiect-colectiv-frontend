import { initialProductsState } from './products-state';
import { createReducer, on } from '@ngrx/store';
import { addProductToShoppingList } from './products-actions';

export const productsReducer = createReducer(
  initialProductsState,
  // Handle successfully loaded todos
  on(addProductToShoppingList, (state, { product }) => ({
    ...state,
    products: [...state.products, product],
    error: null,
    status: 'success',
  }))
);
