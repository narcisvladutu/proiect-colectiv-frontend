import { IProductsState } from './products-state';
import {createSelector} from '@ngrx/store';
import { AppState } from '../state/app.state';

export const selectProducts = (state: AppState) => state.products;
export const selectAllProducts = createSelector(
  selectProducts,
  (state: IProductsState) => state.products
);